//
//  RecordedAudio.swift
//  Pitch Perfect
//
//  Created by Eliud Ortiz on 2/26/16.
//  Copyright © 2016 Eliud Ortiz. All rights reserved.
//

import Foundation

class RecordedAudio: NSObject {
    
    var filePathUrl : NSURL!
    var title: String!
    
    init(filePathUrl: NSURL, title: String) {
        self.filePathUrl = filePathUrl
        self.title = title
    }
}

